cmake_minimum_required(VERSION 3.9.0)
project(bosch VERSION 0.1.0 LANGUAGES CXX)

add_library(MyQueue include/myqueue.hpp src/myqueue.cpp)

add_executable(MyMain src/main.cpp) 

target_link_libraries(MyMain MyQueue)

enable_testing()
find_package(GTest REQUIRED)
include(GoogleTest)

add_executable(MyQueueTest test/utest-myqueue.cpp)
target_link_libraries(MyQueueTest ${GTEST_BOTH_LIBRARIES})
gtest_discover_tests(MyQueueTest)