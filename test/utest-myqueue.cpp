#include <gtest/gtest.h>
#include "../include/myqueue.hpp"
#include "../src/myqueue.cpp"

using namespace testing;

class QueueTest : public Test
{
protected:
    void SetUp() override
    {
        // q0_ remains empty
        q.Push(1);
        q.Push(2);
        q.Push(3);
    }

    // void TearDown() override {}

    Queue<int> q0{1};
    Queue<int> q{10};
};

TEST_F(QueueTest, IsEmptyInitially)
{
    EXPECT_EQ(q0.Count(), 0);
}

TEST_F(QueueTest, IsNotEmptyInitially)
{
    EXPECT_GT(q.Count(), 0);
}

TEST_F(QueueTest, PopElement)
{
    EXPECT_EQ(q.Pop(), 1);
    EXPECT_EQ(q.Count(), 2);
}

TEST_F(QueueTest, PushElement)
{
    q.Push(4);
    EXPECT_EQ(q.Count(), 4);
}

TEST_F(QueueTest, QueueSize)
{
    EXPECT_EQ(q.Size(), 10);
    q.Push(4);
    EXPECT_EQ(q.Size(), 10);
}

int main(int argc, char **argv)
{
    InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}