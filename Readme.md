## Multi-threaded element queue in C++

# Requirements

Develop a class from scratch to queue a finite number of elements. This class will be used for multi-threaded communication as follows:
- Reading thread pops elements while writing thread pushes elements.
- If the queue is empty, reading thread will block and wait for the next element.
- If the queue is full, writing thread will block and wait for another thread to remove an item.

# How to Build

```console
mkdir build
cd build
cmake ..
make
```

# How to Run

To run a two thread example (build folder):

```console
./MyMain
```

To run unit tests (build folder):

```console
./MyQueueTest
```
