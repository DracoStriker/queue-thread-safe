#ifndef _MYQUEUE_H_
#define _MYQUEUE_H_

#include <queue>
#include <mutex>
#include <condition_variable>

/**
 * @brief This class template is a thread safe implementation of Queue with a limited size.
 *
 * @tparam T Type of the elements to be stored.
 */
template <typename T>
class Queue
{
public:
    /**
     * @brief Construct a new Queue object.
     *
     * @param size max size of the queue.
     */
    Queue(int size);
    /**
     * @brief This method adds a new element to the end of the Queue.
     * If Queue is full thread will wait until element is poped.
     *
     * @param element element to insert.
     */
    void Push(T element);
    /**
     * @brief This method removes the first element of the Queue.
     * If Queue is empty thread will wait until element is pushed.
     *
     * @return The first element of the Queue that was just removed.
     */
    T Pop();
    /**
     * @brief This method returns the current number of elements stored in Queue.
     *
     * @return The amount of elements stored.
     */
    int Count();
    /**
     * @brief This method returns the max size of the Queue.
     *
     * @return The max number of elements.
     */
    int Size();

private:
    int size;
    std::queue<T> q;
    std::mutex m;
    std::condition_variable cv;
};

#endif