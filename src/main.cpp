/**
 * @file main.cpp
 * @author Simão Reis (simao.reis@outlook.pt)
 * @brief Simple test for Queue.
 * @version 0.1
 * @date 2024-02-05
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#include <thread>
#include <chrono>
#include <iostream>
#include "../include/myqueue.hpp"
#include "myqueue.cpp"

using namespace std;

Queue<int> q(2);

void write()
{
    for (int i = 0; i < 5; i++)
    {
        q.Push(i);
        cout << "Push(" << i << ")" <<  endl;
    }
}

void read()
{
    for (int i = 0; i < 5; i++)
    {
        this_thread::sleep_for(std::chrono::seconds(1));
        int n = q.Pop();
        cout << "Pop(" << n << ")" <<  endl;
    }
}

int main()
{
    thread writing(write);
    thread reading(read);
    writing.join();
    reading.join();
    return 0;
}