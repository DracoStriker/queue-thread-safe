#include "../include/myqueue.hpp"

using namespace std;

template <typename T>
Queue<T>::Queue(int size)
{
    this->size = size;
}

template <typename T>
void Queue<T>::Push(T element)
{
    unique_lock<mutex> lock(m);
    cv.wait(lock, [this]
            { return q.size() < size; });
    q.push(element);
    cv.notify_one();
}

template <typename T>
T Queue<T>::Pop()
{
    unique_lock<mutex> lock(m);
    cv.wait(lock, [this]
            { return q.size() > 0; });
    T element = q.front();
    q.pop();
    cv.notify_one();
    return element;
}

template <typename T>
int Queue<T>::Count()
{
    unique_lock<mutex> lock(m);
    return q.size();
}

template <typename T>
int Queue<T>::Size()
{
    return size;
}
